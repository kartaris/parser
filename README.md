# **Описание алгоритма** #

1. Создается объект класса *CDownload*, который записывает содержимое веб-страницы во временный файл с помощью метода *CreateHtmlFile* класса CDownload(для подключения к серверу используется стандартная библиотека *WinInet*), путь к которому и имя извлекаются методом *CutUrl* класса *CDownload*.

1.  Создается объект класса *CGetText*, служащий для получения "полезной" информации из записанного на предыдщем этапе временного файла. Содержимое файла записывается в строку "txt", временный файл удаляется, а полученная строка передается для изменения в метод *DeleteTags*. DeleteTags работает с использованием стандартной библиотеки regex в три этапа: удаляются все "лишние" теги; работа с заголовками, параграфами и переносами строк; оформление ссылок. После запускается метод *WriteToFile*, который записывает конечный вариант строки "txt" в файл "название_страницы.txt"/*Метод FormatString не запускается, так как не дописан и в результате его работы появляются с кодировкой файла*/

1. Созданные объекты удаляюся, завершая алгоритм.

/* Начата реализация класса CConf */
/* Работа алгоритма проверена на сайтах lenta.ru и gazeta.ru. В первом случае извлечена максимально полезная информация, во втором же присутствуют некоторые теги, которые также можно удалить добавив в шаблон по умолчанию(или в файл конфигурации после завершения работы над классом CConf). */
 