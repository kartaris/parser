#include "gettext.h"

void CGetText::DeleteTags(string & s)
{
	using namespace regex_constants; //������ ������������ ���� ��� ������������� �������� ���������� regex
	//������ ������ ��� ������ ������ �����, ���� icase �������� �������������� � ��������
	regex RX("<!DOCTYPE(>| .*?>)|<html(>| .*?>)|</html>|<body(>| .*?>)|</body>|<head(>| .*?>)(.*?)</head>|<script( .*?>|>).*?</script>|<link .*?/>|<nav(>| .*?).*?</nav>|<section(>| .*?).*?</section>|</section>|<div(>| .*?>)|</div>|<!--.*?-->|<form(>| .*?).*?</form>|<img .*?/>|<footer.*",icase);
	//�������� ����
	s = regex_replace(s, RX, "");
	RX = "<time(>| .*?>)|</time>|<br>|<p(>| .*?>)|</p>|<h1(>| .*?>)|</h1>";
	s = regex_replace(s, RX, "\n");
	RX = "</a>";
	s = regex_replace(s, RX, " ]");
	RX = "<a( | .*?)href=\"";
	s = regex_replace(s, RX, "[ ");
	RX = "\".*?>";
	s = regex_replace(s, RX, " | ");
}

void CGetText::FormatString(string & s)
{
	if (s.length() > 80) {
		int i = s.length()/80;
		for (int j = 1; j < i; j++)
		{
			s.insert(j * 80, "\n");
		}
	}
}

void CGetText::WriteToFile(string s, char *t)
{
	locale loc("Russian");//�����������
	ofstream out(".\\" + input->path + input->name + t);
	out.imbue(loc);//������ ������ ��� ��������� �����
	int i;
	string tmp = "", txt = "";
	for (int i = 0; i < s.length(); i++) 
	{
		while (s[i] != '\n' && i<s.length()) {
			tmp = tmp + s[i];
			i++;
		}
		//FormatString(tmp);
		txt = txt + tmp + '\n';
		tmp = "";
	}
	out << txt;
	out.close();
}

CGetText::CGetText(CDownload * d)
{
	input = d;
	char c;
	string txt="";
	int i=0;
	ifstream in(".\\" + input->path + input->name + "tmp");
	while (!in.eof())
	{
		c = in.get();
		if (c!='\n') txt = txt + c;
	}
	in.close();
	remove((".\\" + input->path + input->name + "tmp").c_str());
	DeleteTags(txt);
	WriteToFile(txt);
}

CGetText::~CGetText()
{
}

/*string CGetText::GetFromTag(string tname)
{
string txt="", tmp, opening_tag= "$OPENING_TAG_" + tname + "$", closing_tag= "$CLOSING_TAG_" + tname + "$";
MarkTags(tname);
NumberTags(tname);

/*while (!in.eof()) {
getline(in, tmp);
int to = tmp.find(opening_tag);
if (t == 0)
if (to >= 0)
{
tmp.erase(to, opening_tag.length());
t = 1;
}
if (t == 1)
{
int tc = tmp.find(closing_tag);
if (tc >= 0) {
tmp.erase(tc, closing_tag.length());
t = 2;
if (tmp.length()!=0) txt = txt + tmp + "\n";
}
}
if (t == 2)
{
txt = txt + GetFromTag(tname);
t = 0;
}
if (t!=0 && tmp.length() > 0) txt = txt + tmp + "\n";
}
return txt;
}

void CGetText::MarkTags(string tname)
{
string tmp, opening_tag = "\n$OPENING_TAG_" + tname +"$\n", closing_tag = "\n$CLOSING_TAG_" + tname + "$\n";
regex TO("<" + tname + "(>| .*?>)"), TC("</" + tname + ">"); //������� ������
ifstream in(".\\" + input->path + input->name + "tmp");
ofstream out(".\\" + input->path + input->name + "marked");
while (!in.eof())
{
getline(in, tmp);
tmp = regex_replace(tmp, TO, opening_tag);	//��������
tmp = regex_replace(tmp, TC, closing_tag);
out << tmp << endl;
}
in.close();
out.close();
}

void CGetText::NumberTags(string tname) {
ifstream in(".\\" + input->path + input->name + "marked");
ofstream out(".\\" + input->path + input->name + "tmp");
string tmp, opening_tag = "$OPENING_TAG_" + tname + "$", closing_tag = "$CLOSING_TAG_" + tname + "$";
int n = 0, m = 0, c;
vector <int> map;
while (!in.eof())
{
getline(in, tmp);
int to = tmp.find(opening_tag);
if (to >= 0) {
n++;
m++;
map.resize(m);
map[m-1] = n;
char s[10];
_itoa_s(n, s, 10);
tmp.replace(to, opening_tag.length(), "$OPENING_TAG_" + tname + "$$" + s + "$");
}
else {
int tc = tmp.find(closing_tag);
if (tc >= 0) {
char s[10];
_itoa_s(map[m-1], s, 10);
tmp.replace(tc, closing_tag.length(), "$CLOSING_TAG_" + tname + "$$" + s + "$");
m--;
}
}
out << tmp << endl;
}
out.close();
in.close();
remove((".\\" + input->path + input->name + "marked").c_str());
}
*/
