#pragma once
#include "download.h"
#include <regex>
#include <vector>

using namespace std;

class CGetText
{
private:
	CDownload *input;
	void DeleteTags(string & s);
	void FormatString(string & s);
	//string GetFromTag(string tname);				//����� "��������" ����� �� ����� ���������� ���� � ���������� ��� � ���� ������
	//void MarkTags(string tname);					//����� ��������� ���� ���������� ���� � ����� ������� ��� �������� �������
	//void NumberTags(string tname);					//����� �������� ���� ���������� ���� ��� ���������� ��������, 
													//��� ������������� ���������� ��������� �����
	void WriteToFile(string s,					//����� ���������� ������ � ���� � �����������,
					 char *t="txt");				//������������� � ���������� t(�� ��������� txt)
public:
	CGetText(CDownload *d);
	~CGetText();
};

