#include <iostream>
#include "gettext.h"

using namespace std;

int main() {
	string s;
	cin >> s;
	CDownload *d = new CDownload(s);
	CGetText *t = new CGetText(d);
	delete d, t;
	return 0;
}